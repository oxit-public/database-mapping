# oxit/database-mapping

Inspirace z [Nette blogu](https://forum.nette.org/cs/34856-planuje-se-exploreru-pridat-podporu-datovych-typu-jako-to-maji-formulare-a-latte) na rozšíření objekt ActiveRow z nette/database


### Základní instalace

```
extensions:
    netteDatabase: Oxit\Msgraph\DI\NetteDatabaseExtension
```

## Výchozí objekt

vraci misto ActiveRow

```
netteDatabase:
	baseRow: App\Model\BaseRow
```

## Mapování objektů

```
netteDatabase:
	mapping:
		articles: App\Model\Article
		images: App\Model\Image
```

## Více databází

V parametru databases napiste indexy jako u databáze

```
database:
	default:
		dsn: 'mysql:host=127.0.0.1;dbname=east_port'
		user: root
		password:
	dalsiNazev:
		dsn: 'mysql:host=127.0.0.1;dbname=intranet'
		user: root
		password:		
			
netteDatabase:
    databases: {default, dalsiNazev atd.}

services:		
	- \App\Sluzba(@database.default.explorer, @database.dalsiNazev.explorer)
```

Od toho se odvíjí i úprava mappingu

```
netteDatabase:
	mapping:
	    default:
                images: App\Model\Image
	    dalsiNazev:
                articles: App\Model\Article
```

Poté použití jako do posud

```
services:		
	- \App\Sluzba(@database.default.explorer, @database.dalsiNazev.explorer)
```