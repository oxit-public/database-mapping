<?php

namespace Oxit\DatabaseMapping\Model;

use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;

final class CustomSelection extends Selection
{
	/** @var string[] */
	private array $classMapping = [];

	private ?string $baseRow = null;

	/** @param string[] $mapping @internal */
	public function setClassMapping(array $mapping): void
	{
		$this->classMapping = $mapping;
	}

	/** @internal */
	public function setBaseRow(?string $baseRow): void
	{
		$this->baseRow = $baseRow;
	}

	/*
	 * Edited base functions
	 */

	public function createSelectionInstance(?string $table = null): self
	{
		$selection = new self($this->explorer, $this->conventions, $table ?: $this->name, $this->cache ? $this->cache->getStorage() : null);

		$selection->setClassMapping($this->classMapping);
		$selection->setBaseRow($this->baseRow);

		return $selection;
	}

	protected function createGroupedSelectionInstance(string $table, string $column): CustomGroupSelection
	{
		$group = new CustomGroupSelection($this->explorer, $this->conventions, $table, $column, $this, $this->cache ? $this->cache->getStorage() : null);

		$group->setBaseRow($this->baseRow);
		$group->setClassMapping($this->classMapping);

		return $group;
	}

	protected function createRow(array $row): ActiveRow
	{
		$className = $this->classMapping[$this->getName()] ?? ($this->baseRow ?? ActiveRow::class);

		return new $className($row, $this);
	}
}

