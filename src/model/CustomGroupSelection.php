<?php

namespace Oxit\DatabaseMapping\Model;

use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\GroupedSelection;

final class CustomGroupSelection extends GroupedSelection
{
	/** @var string[] */
	private array $classMapping = [];

	private ?string $baseRow = null;

	/** @param string[] $mapping */
	public function setClassMapping(array $mapping): void
	{
		$this->classMapping = $mapping;
	}

	public function setBaseRow(?string $baseRow): void
	{
		$this->baseRow = $baseRow;
	}

	/*
	 * Edited base functions
	 */

	public function createSelectionInstance(?string $table = null): CustomSelection
	{
		$selection = new CustomSelection($this->explorer, $this->conventions, $table ?: $this->name, $this->cache ? $this->cache->getStorage() : null);

		$selection->setClassMapping($this->classMapping);
		$selection->setBaseRow($this->baseRow);

		return $selection;
	}

	protected function createRow(array $row): ActiveRow
	{
		$className = $this->classMapping[$this->getName()] ?? ($this->baseRow ?? ActiveRow::class);

		return new $className($row, $this);
	}
}

