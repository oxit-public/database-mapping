<?php

namespace Oxit\DatabaseMapping\Model;


use Nette\Database\Explorer;

final class Database extends Explorer
{
	/** @var string[] */
	private array $classMapping = [];

	private ?string $baseRow = null;

	public function table(string $table): CustomSelection
	{
		$selection = new CustomSelection($this, $this->getConventions(), $table);

		if ($this->classMapping) {
			$selection->setClassMapping($this->classMapping);
			$selection->setBaseRow($this->baseRow);
		}

		return $selection;
	}

	/** @param string[] $mapping @internal */
	public function setClassMapping(array $mapping): void
	{
		$this->classMapping = $mapping;
	}

	/** @internal */
	public function setBaseRow(?string $baseRow): void
	{
		$this->baseRow = $baseRow;
	}
}
