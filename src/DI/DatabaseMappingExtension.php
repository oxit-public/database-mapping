<?php

namespace Oxit\DatabaseMapping\DI;

use Nette;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Oxit\DatabaseMapping\Model\Database;

/**
 * @property \stdClass $config
 */
class DatabaseMappingExtension extends CompilerExtension
{

	public function getConfigSchema(): Nette\Schema\Schema
	{
		return Expect::structure([
			'mapping' => Expect::array(),
			'baseRow' => Expect::string(),
			'databases' => Expect::mixed()->default('default'),
		]);
	}

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		if (is_string($this->config->databases)) {
			if (!isset($this->config->mapping[$this->config->databases])) {
				$this->config->mapping[$this->config->databases] = $this->config->mapping;
			}
			$databases = [$this->config->databases];
		} elseif (is_array($this->config->databases)) {
			$databases = $this->config->databases;
		} else {
			$databases = [];
		}

		$cacheStorage = $builder->getDefinition("cache.storage");

		foreach ($databases as $database) {  // prepsani databazovych sluzeb

			$connection = $builder->getDefinition("database.$database.connection");
			$structure = $builder->getDefinition("database.$database.structure");
			$conventions = $builder->getDefinition("database.$database.conventions");

			$databaseService = $builder->getDefinition("database.$database.explorer")
				->setFactory(Database::class, [$connection, $structure, $conventions, $cacheStorage]);

			if (!empty($this->config->mapping[$database])) {
				$databaseService->addSetup('setClassMapping', [$this->config->mapping[$database]]);
			}

			if ($this->config->baseRow) {
				$databaseService->addSetup('setBaseRow', [$this->config->baseRow]);
			}
		}
	}
}
